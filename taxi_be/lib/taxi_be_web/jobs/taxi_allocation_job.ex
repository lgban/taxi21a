defmodule TaxiBeWeb.TaxiAllocationJob do
  use GenServer

  def start_link(request, name) do
    GenServer.start_link(__MODULE__, request, name: name)
  end

  def init(request) do
    Process.send(self(), :step1, [:nosuspend])
    {:ok, %{request: request}}
  end

  def compute_ride_fare(request) do
    %{
      "pickup_address" => pickup_address,
      "dropoff_address" => dropoff_address
     } = request

    {:ok, coord1} = TaxiBeWeb.Geolocator.geocode(pickup_address)
    {:ok, coord2} = TaxiBeWeb.Geolocator.geocode(dropoff_address)
    {distance, _duration} = TaxiBeWeb.Geolocator.distance_and_duration(coord1, coord2)
    {request, Float.ceil(distance/300)}
  end

  def notify_customer_ride_fare({request, fare}) do
    %{"username" => customer} = request
   TaxiBeWeb.Endpoint.broadcast("customer:" <> customer, "booking_request", %{msg: "Ride fare: #{fare}"})
  end

  def select_candidate_taxis(%{"pickup_address" => _pickup_address}) do
    [
      %{nickname: "frodo", latitude: 19.0319783, longitude: -98.2349368},
      %{nickname: "samwise", latitude: 19.0061167, longitude: -98.2697737},
      %{nickname: "merry", latitude: 19.0092933, longitude: -98.2473716}
    ] |> Enum.shuffle()
  end

  def handle_info(:step1, %{request: request} = state) do

    task = Task.async(fn -> select_candidate_taxis(request) end)

    compute_ride_fare(request)
    |> notify_customer_ride_fare()

    candidates = Task.await(task)

    {timer, taxi} = intermediate(request, candidates)

    {:noreply, %{request: request, contacted_taxi: taxi, candidates: candidates, timer: timer}}
  end

  def intermediate(request, candidates) do
    # Select a taxi
    taxi = hd(candidates)

    # Forward request to taxi driver
    %{
      "pickup_address" => pickup_address,
      "dropoff_address" => dropoff_address,
      "booking_id" => booking_id
    } = request
    TaxiBeWeb.Endpoint.broadcast(
      "driver:" <> taxi.nickname,
      "booking_request",
        %{
          msg: "Viaje de '#{pickup_address}' a '#{dropoff_address}'",
          bookingId: booking_id
        })
    {Process.send_after(self(), :timeout, 5_000), taxi}
  end

  def handle_info(:timeout, %{request: request, candidates: candidates} = state) do
    IO.inspect("Tu tiempo termino")
    # Discard taxi driver
    candidates = tl(candidates)
    {timer, taxi} = intermediate(request, candidates)
    {:noreply, %{state | timer: timer, candidates: candidates, contacted_taxi: taxi}}
  end

  def handle_cast({:acceptance, req}, %{timer: timer} = state) do
    IO.inspect(state)
    if timer != nil do
      Process.cancel_timer(timer)
    end
    TaxiBeWeb.Endpoint.broadcast("customer:" <> state.request["username"], "booking_request", %{msg: "Tu taxi esta en camino"})
    {:noreply, %{state | timer: nil}}
  end
end
